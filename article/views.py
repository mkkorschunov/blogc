from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.shortcuts import redirect 
from .models import Article
def archive(request):
 return render(request, 'archive.html', {"posts": Article.objects.all()})
def create_post(request):
    B=request.user.is_authenticated
    if   ( B ):
        # Здесь будет основной код представления
        if request.method == "POST":
            # обработать данные формы, если метод POST
            form = { 'text': request.POST["text"], 'title': request.POST["title"] }
            # в словаре form будет храниться информация, введенная пользователем
            if form["text"] and form["title"]:
                # если поля заполнены без ошибок
                Article.objects.create(text=form["text"], title=form["title"], author=request.user) 
                #return redirect('get_article', article_id=Article.id)
                return HttpResponseRedirect("/")
                # перейти на страницу поста
            else:
                # если введенные данные некорректны
                form['errors'] = u"Не все поля заполнены"
                return render(request, 'create_post.html', {'form': form})
        else:
             # просто вернуть страницу с формой, если метод GET
            return render(request, 'create_post.html', {})
    else:
        return HttpResponseNotFound("<h1>Page not found</h1>")    